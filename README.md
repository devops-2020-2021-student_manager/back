**Faire un docker mySQL :**    
$ docker pull mysql  
$ docker run --name db_studentmanager -itd -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_DATABASE=studentmanager mysql  
$ mysql -u root -p -h 127.0.0.1 studentmanager < gestionnaire.sql  
_(password : my-secret-pw)_  

**Lancer l'API**  

(Le port du serveur peut être changer dans api/src/main/ressources/application.properties)  
(Le user / password / dbname peuvent être changer dans api/src/main/ressources/bdd.properties)  

Se placer dans le dossier api:  
$ mvn clean install

Une fois le dossier target généré, faire :  
$ java -jar target/api-0.0.1-SNAPSHOT.jar 
  
  
Route disponible :

**GET /eleves/all**  
Pour afficher tous les élèves

**GET /eleves/find/{id}**  
Pour afficher un élève grace à son id ({id} a remplacer )  

**GET /eleves/delete/{id}**
Supprime un eleve par son id

**POST /eleves/create**  
Pour ajouter un élève  

JSON Template :  
{  
    "user":"math",
    "name":"gonzalez",
    "firstname":"mathieu",
    "profil":"non",
    "spec":"dev",
    "email":"gonzalezmath3000@gmail.com",
    "password" :"1234test",
    "remember_token" : "azertyf",
    "role" : "professeur"
}  

**POST /eleves/verif**  
Pour récupéré le token  

JSON Template  
{  
    "user":"rapsal",
    "password" :"$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"
}  



