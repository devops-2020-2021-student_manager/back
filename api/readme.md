étape pour lancer l'api

- on peut configuré le port de l'api dans application.properties
- on peut aussi configuré le chemin de la bdd le chemin par défaut et en local et attention au nom de la bdd dans bdd.properties
- on peut mettre le login mot de passe de la bdd aussi dans bdd.properties

les .properties sont dans src/main/resources

une fois la configuration fait on peut pakagé le livrable avec un 
mvn clean install

commande a faire avec le cmd dans back/api et le livrable et déployer dans target en .jar

puis lancé l'api avec un 

java -jar api-0.0.1-SNAPSHOT.jar 

Route disponible actuellement

get /eleves/all
pour affiché tout les élève

get /eleves/find/{id} 
pour affiché un elève grace a son id ({id} a remplacé )

post /eleves/create
pour ajouté qqun

il faut que le corp de la requête ressemble a ça :
```json
{
        "nom":"mathieu",
        "mail":"gonzalezmath3@gmail.com",
        "password" :"1234test",
        "role" : "professeur"
    
}
```

post 127.0.0.1:9090/eleves/verif
pour récupéré le token 
il faut que le corp de la requête ressemble a ça :
```json
{
        "nom":"admin",
        "password" :"azerty123"  
}
```

get /eleves/delete/{id}
suprime un eleve par sont id



## Déploiement sur registry

```
mvn deploy -s settings.xml
```