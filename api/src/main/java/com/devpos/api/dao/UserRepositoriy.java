package com.devpos.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devpos.api.users.Users;

public interface UserRepositoriy extends JpaRepository<Users, Integer>{
	Users findById(int id);
	List<Users>  OrderByName();
	
	
	@Query(value = "SELECT remember_token FROM users WHERE 	user = :user AND password = :password",nativeQuery=true)
	String verifEleve(@Param("user") String user,@Param("password") String password);		
	

}
