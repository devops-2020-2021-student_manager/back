package com.devpos.api.controleur;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devpos.api.dao.UserRepositoriy;
import com.devpos.api.users.Users;

@RestController
public class UserControleur {
	@Autowired
	UserRepositoriy userRepository;
	
	@GetMapping(value="/eleves/all", produces = MediaType.APPLICATION_JSON_VALUE)
	    public List<Users> listEleves() {		 
	        return userRepository.findAll();
	    }
	 
	@GetMapping(value = "/eleves/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	  public Users findEleveById (@PathVariable int id) {
		return userRepository.findById(id);
	  
	}
	
	@GetMapping(value = "/eleves/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	  public void deleteEleveById (@PathVariable int id) {
		userRepository.deleteById(id);
	  
	}
	 
	
	@PostMapping(value = "/eleves/create", consumes = MediaType.APPLICATION_JSON_VALUE)
	  public void createEleve (@RequestBody Users users) {
		userRepository.save(users);
	  
	}
	
	@PostMapping(value = "/eleves/verif")
	  public String verifEleve (@RequestBody Users users) {
		return userRepository.verifEleve(users.getUser(),users.getPassword());
	}
	 

}
