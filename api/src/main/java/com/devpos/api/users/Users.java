package com.devpos.api.users;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Users {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private int id;
	private String user;
	private String name;
	private String firstname;
	private String profil;
	private String spec;
	private String email;
	private String role;
	private String password;
	private String remember_token;
	
	public Users(String nom,String mail,String password, String role) {
		this.name=nom;
		this.email=mail;
		this.password=password;
		this.role=role;
	}
	
	
	

}
